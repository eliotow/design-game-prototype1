﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightTrigger : MonoBehaviour {
    public GameObject player;

    public float speedboost;
    
 
	// Use this for initialization
	void Start () {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "player")
        {
           
            player.GetComponent<playercontroller>().speed += speedboost;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
